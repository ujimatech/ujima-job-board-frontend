import Vue from "vue";
import Router from "vue-router";
import mainRoutes from "../router/mainNav"
import dashboardRoutes from "../router/dashboard"

Vue.use(Router);

var allRoutes = []
allRoutes = allRoutes.concat(mainRoutes, dashboardRoutes)
const routes = allRoutes
const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router
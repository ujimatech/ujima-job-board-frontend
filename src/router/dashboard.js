import Dashboard from "@/components/Profile/Dashboard.vue";
import EmployerDashboard from "@/components/Profile/Employer/EmployerDashboard.vue";
import EmployerProfile from "@/components/Profile/Employer/EmployerProfile.vue";
import EmployerPost from "@/components/Profile/Employer/EmployerPost.vue";
import EmployerResume from "@/components/Profile/Employer/EmployerResume.vue";
import EmployerPackages from "@/components/Profile/Employer/EmployerPackages.vue";
import EmployerManageJobs from "@/components/Profile/Employer/EmployerManageJobs.vue";

const dashboardRoutes = [
    {
      path: "/dashboard",
      component: Dashboard,
      children: [
          {
              path: '',
              component: EmployerDashboard
          },
            {
              path: "/dashboard/profile",
              component: EmployerProfile,
            },
            {
              path: "/dashboard/jobs",
              component: EmployerManageJobs
            },
            {
              path: "/dashboard/jobs/post",
              component: EmployerPost,
            },
            {
              path: "/dashboard/candidates",
              component: EmployerResume,
            },
      ]
    },
    // {
    //   path: "/dashboard/profile",
    //   component: EmployerProfile,
    // },
]

export default dashboardRoutes
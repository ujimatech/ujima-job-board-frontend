import Home from "@/views/Home.vue";
import Search from "@/views/Search.vue";
import Job from "@/views/Job.vue";
import Login from "@/views/Login.vue";


const mainRoutes = [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/jobs",
      name: "searchjobs",
      component: Search,
      props: {
        searchType: "job"
      }
    },
    {
      path: "/jobs/:jobid",
      name: "jobdetails",
      component: Job,
    },
    {
      path: "/contracts",
      name: "searchcontracts",
      component: Search,
      props: {
        searchType: "contract"
      }
    },
    // {
    //   path: "/about",
    //   name: "about",
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () =>
    //     import(/* webpackChunkName: "about" */ "./views/About.vue")
    // }
  ]

export default mainRoutes
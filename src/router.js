import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home.vue";
import Search from "@/views/Search.vue";
import Job from "@/views/Job.vue";
import Login from "@/views/Login.vue";

import EmployerDashboard from "@/components/Profile/EmployerDashboard.vue";
import EmployerProfile from "@/components/Profile/EmployerProfile.vue";
import EmployerPost from "@/components/Profile/EmployerPost.vue";
import EmployerResume from "@/components/Profile/EmployerResume.vue";
import EmployerPackages from "@/components/Profile/EmployerPackages.vue";
import EmployerManageJobs from "@/components/Profile/EmployerManageJobs.vue";

import CandidateDashboard from "@/components/Profile/candidate/CandidateDashboard.vue";
import CandidateProfile from "@/components/Profile/candidate/CandidateProfile.vue";
import CandidateResume from "@/components/Profile/candidate/CandidateResume.vue";
import CandidateAppliedJobs from "@/components/Profile/candidate/CandidateAppliedJobs.vue";
import CandidateResumeManager from "@/components/Profile/candidate/CandidateResumeManager.vue";
import CandidateFavouriteJobs from "@/components/Profile/candidate/CandidateFavouriteJobs.vue";
import CandidateMessage from "@/components/Profile/candidate/CandidateMessage.vue";
import CandidateReview from "@/components/Profile/candidate/CandidateReview.vue";
import CandidateJobAlerts from "@/components/Profile/candidate/CandidateJobAlerts.vue";
import CandidateChangePassword from "@/components/Profile/candidate/CandidateChangePassword.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/jobs",
      name: "searchjobs",
      component: Search,
      props: {
        searchType: "job"
      }
    },
    {
      path: "/jobs/:jobid",
      name: "jobdetails",
      component: Job,
    },
    {
      path: "/contracts",
      name: "searchcontracts",
      component: Search,
      props: {
        searchType: "contract"
      }
    },
    {
      path: "/employer_dashboard",
      name: "dashboard",
      component: EmployerDashboard
    },
    {
      path: "/employer_profile",
      name: "profile",
      component: EmployerProfile
    },
    {
      path: "/employer_post",
      name: "post",
      component: EmployerPost
    },
    {
      path: "/employer_resume",
      name: "resume",
      component: EmployerResume
    },
    {
      path: "/employer_manage_jobs",
      name: "parkages",
      component: EmployerManageJobs
    },
    {
      path: "/employer_packages",
      name: "parkages",
      component: EmployerPackages
    },
    {
      path: "/candidate_dashboard",
      name: "candidate_dashboard",
      component: CandidateDashboard
    },
    {
      path: "/candidate_profile",
      name: "candidate_profile",
      component: CandidateProfile
    },
    {
      path: "/candidate_resume",
      name: "candidate_resume",
      component: CandidateResume
    },
    {
      path: "/candidate_applied_jobs",
      name: "candidate_applied_jobs",
      component: CandidateAppliedJobs
    },
    {
      path: "/candidate_resume_manager",
      name: "candidate_resume_manager",
      component: CandidateResumeManager
    },
    {
      path: "/candidate_favourite_jobs",
      name: "candidate_favourite_jobs",
      component: CandidateFavouriteJobs
    },
    {
      path: "/candidate_messages",
      name: "candidate_messages",
      component: CandidateMessage
    },
    {
      path: "/candidate_review",
      name: "candidate_review",
      component: CandidateReview
    },
    {
      path: "/candidate_job_alerts",
      name: "candidate_job_alerts",
      component: CandidateJobAlerts
    },
    {
      path: "/candidate_change_password",
      name: "candidate_change_password",
      component: CandidateChangePassword
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    }
  ]
});

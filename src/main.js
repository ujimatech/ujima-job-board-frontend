import "@babel/polyfill";
import "mutationobserver-shim";
import router from "./router";
import Vue from "vue";
import App from "./App.vue";
import './plugins';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar, faStarHalfAlt, faUserCheck, faFile, faEnvelopeOpenText, faTrashAlt, faColumns, faIdBadge, faTasks, faBoxOpen, faCommentsDollar, faUnlockAlt, faSignOutAlt, faPaperPlane, faBell, faUserTag, faMapMarkerAlt, faBriefcase, faCalendarTimes, faEye, faPencilAlt, faComments } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faStar, faStarHalfAlt, faUserCheck, faFile, faEnvelopeOpenText, faTrashAlt, faColumns, faIdBadge, faTasks, faBoxOpen, faCommentsDollar, faUnlockAlt, faSignOutAlt, faPaperPlane, faBell, faUserTag, faMapMarkerAlt, faBriefcase, faCalendarTimes, faEye, faPencilAlt, faComments )

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
